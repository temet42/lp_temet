<!doctype html>
<html lang="">
    <head>
        <style>
            *Box-style
**/

.box {
    padding: 50px 30px;
    text-align: center;
    -webkit-box-shadow: 0 0 0 0 #ffffff;
    box-shadow: 0 0 0 0 #ffffff;
    -webkit-transition: 0.3s;
    transition: 0.3s;
    border-radius: 5px;
    -webkit-transform: translateY(0);
    transform: translateY(0)
}

.box:hover {
    -webkit-box-shadow: 0 10px 30px -5px rgba(0, 0, 0, 0.1);
    box-shadow: 0 10px 30px -5px rgba(0, 0, 0, 0.1);
    background-color: #ffffff;
    -webkit-transform: translateY(-5px);
    transform: translateY(-5px)
}

.box h4 {
    text-transform: uppercase;
}

.box .box-icon {
    height: 160px;
    margin-bottom: 20px;
}

.box .box-icon img {
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.box:hover .box-icon img {
    -webkit-filter: grayscale(0);
    filter: grayscale(0)
}

            </style>
       <body>
       <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="images/portfolio-icon-3.png" alt="">
                        </div>
                        <h3>Mockup</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
    </body>
</html>
