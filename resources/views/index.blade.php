<!DOCTYPE HTML>
<html lang="zxx">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="og:url" content="https://www.temet.com.br">
		<meta property="og:description"content="Desenvolvimentp Ágil”>
		<meta property="og:image" content="https://www.temet.com.br/img.icon.png">
		
		



		<title>TEMET- Desenvolvimento Ágil</title>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="all" />
		<!-- Slick nav CSS -->
		<link rel="stylesheet" type="text/css" href="css/slicknav.min.css" media="all" />
		<!-- Iconfont CSS -->
		<link rel="stylesheet" type="text/css" href="css/icofont.css" media="all" />
		<!-- Slick CSS -->
		<link rel="stylesheet" type="text/css" href="css/slick.css">

		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- Owl carousel CSS -->
		<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
		<!-- Popup CSS -->
		<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
		<!-- Switcher CSS -->
		<link rel="stylesheet" type="text/css" href="css/switcher-style.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" type="text/css" href="css/animate.min.css">
		<!-- Main style CSS -->
		<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
		<!-- Responsive CSS -->
		<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all" />
		<!-- Favicon Icon -->
		<link rel="shortcut icon" type="image/icon" href="img/icon.png" />
		
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

		<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    
    <script type="text/javascritp">
       function enviar(){
        swal("Good job!", "You clicked the button!", "success");

       }
    </script>	
	</head>

	<body data-spy="scroll" data-target=".header" data-offset="50">
		<!-- Page loader -->
	    <div id="preloader"></div>
		<!-- header section start -->
		<header class="header">
			<div class="container">
				<div class="row flexbox-center">
					<div class="col-lg-10 col-md-9 col-sm-12">
						<div class="responsive-menu"></div>
					    <div class="mainmenu">
                            <ul id="primary-menu">
                                <li><a class="nav-link" href="#home">Home</a></li>
                                <li><a class="nav-link" href="#servicos">Serviços</a></li>
                                <li><a class="nav-link" href="#screenshot">Nossos Produtos</a></li>
								<li><a class="nav-link" href="#scrum">Como Fazemos</a></li>
								<li><a class="nav-link" href="#team">Nosso Time</a></li>
                                <li><a class="nav-link" href="#onde">Onde Estamos</a></li>
                                <li><a class="nav-link" href="#contact">Contato</a></li>
                            </ul>
					    </div>
					</div>
				</div>
			</div>
		</header><!-- header section end -->
        <!-- hero area start -->
       <!-- Header -->
		<header class="banner-temet" id="home">
  			<span class="background"></span>
		</header>

	<!-- about section start--> 
	<section class="about-area ptb-80" id="servicos">
			<div class="container">
				<div class="row">
					<div class="col-lg-4">
					    <div class="single-about-box">
							<i class="icofont icofont-responsive"></i>
							<h4>Sites Responsivos</h4>
							<p  align="justify"> Sites com a tecnologia mais recente do mercado, criando uma versão para cada dispositivo: PC, Tablet e Smartphone..</p>
						</div>
					</div>
					<div class="col-lg-4">
					    <div class="single-about-box ">
							<i class="icofont icofont-computer"></i>
							<h4>Plataforma Web</h4>
							<p  align="justify">Sistemas com funcionalidades exclusivas . Você poderá ter integração com Email Marketing, Redes Sociais e muitas outras ferramentas...</p>
						</div>
					</div>
					<div class="col-lg-4">
					    <div class="single-about-box">
							<i class="icofont icofont-cart"></i>
							<h4>E-commerce</h4>
							<p  align="justify">  Integração com os meios de pagamento .
                        Painel de Controle com acesso restrito e muito mais.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					    <div class="single-about-box">
							<i class="icofont icofont-ruler-pencil"></i>
							<h4>Criação de Logotipos</h4>
							<p  align="justify">Logos profissionais perfeito para uso no seu site ou para enviar aos parceiros comerciais.</p>
						</div>
					</div>
					<div class="col-lg-4">
					    <div class="single-about-box">
							<i class="icofont icofont-smart-phone"></i>
							<h4>Desenvolvimento de Aplicativo</h4>
							<p  align="justify"> Desenvolvimento Híbrido ou nátivo, específica para iOS e Android, o que garante qualidade
                         e melhor desempenho.</p>
						</div>
					</div>
					<div class="col-lg-4" style="margin-top:-1%;">
					    <div class="single-about-box ">
							<i class="icofont icofont-computer"></i>
							<h4>Mockup</h4>
							<p  align="justify">Possibilita a visualização do projeto em um ambiente virtual, viabilizando os possíveis ajustes, antes do início da produção.</p>
						</div>
					</div>
				</div>
			</div>
			
		</section> 
		<!-- about section end -->

		<!-- screenshots section start -->
		<div class="sec-title" >
			<h2>Portifólio<span class="sec-title-border"><span></span><span></span><span></span></span></h2>          
        </div>
		<section class="screenshots-area ptb-30" id="screenshot">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="screenshot-wrap">
							<div class="single-screenshot">
								
								<a href="http://empregosdeti.com/" target=_blank><img src="img/projetos/empregos-de-ti.jpg" alt="screenshot" /></a>
							</div>
							<div class="single-screenshot">
							<a href="http://app.plocar.com.br/" target=_blank><img src="img/projetos/plocar.jpg" alt="screenshot" /></a>
							</div>
							<div class="single-screenshot">
							<a href="http://explorers.digital/" target=_blank><img src="img/projetos/explorers.jpg" alt="screenshot" /></a>
							</div>
						<!--<div class="single-screenshot">
							<a href="http://empregosdeti.com/" target=_blank><img src="images/projetos/empregos-de-ti.jpg" alt="screenshot" /></a>
							</div>-->
							<!--<div class="single-screenshot">
							<a href="http://empregosdeti.com/" target=_blank><img src="images/projetos/empregos-de-ti.jpg" alt="screenshot" /></a>
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- screenshots section end -->

	<section class="about-area ptb-60" id="scrum">
		 <div class="sec-title" >
			<h2>Como Fazemos<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
		
            <img src="img/scrum.jpg" alt="" style="height:100px;">
            <p>  Metodologia de Desenvolvimento ágil</p>        
			<img src="img/metodologia.gif" alt="" style="height:350px;">            
        </div>
			<center>
                <h4>2 semanas</h4><br>
            </center>
		<div class="line col-md-11" style="padding-left:8%;">          
            <svg viewBox="0 0 632 21">
                <defs>
                    <linearGradient x1="-0.826114285%" y1="100%" x2="100%" y2="100%" id="linearGradient-1">
                        <stop stop-color="#3AA7FF" offset="0%"></stop>
                        <stop stop-color="#444" offset="100%"></stop>
                    </linearGradient>
                </defs>
                <g id="Site-Arte" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Home---App" transform="translate(-403.000000, -1443.000000)">
                        <g id="Group-4" transform="translate(401.000000, 1443.000000)">
                            <path d="M326.5,1.16666667 L326.5,19.8333333" id="Line" stroke="#8D61FF" stroke-width="2.337" stroke-linecap="square"></path>
                            <path d="M3.5,1.16666667 L3.5,19.8333333" id="Line" stroke="#444" stroke-width="2.337" stroke-linecap="square"></path>
                            <path d="M632.5,1.16666667 L632.5,19.8333333" id="Line" stroke="#40A2FF" stroke-width="2.337" stroke-linecap="square"></path>
                            <rect id="Rectangle-29" fill="url(#linearGradient-1)" transform="translate(318.000000, 1.000000) scale(-1, 1) translate(-318.000000, -1.000000) " x="3" y="0" width="630" height="2"></rect>
                        </g>
                    </g>
                </g>
            </svg>
		</div>
    
		<!-- about section start--> 
		
			<div class="container">
				<div class="row">
					<div class="col-lg-4">
					    <div class="box ">
							<div class="box-icon">
								<img src="img/planning.gif" alt="" style="height:150px;">
							</div>
							<h4>Planejamento</h4>
							<p >Você e a equipe vão decidir o que vai ser realizado para a proxima sprint.</p>
						</div>
					</div>
					<div class="col-lg-4" >
					    <div class="box">
							<div class="box-icon">
								<img src="img/dev.jpg" alt="" style="height:200px; margin-top:-12%;">
							</div>
						<h4>Desenvolvimento</h4>
                        <p>A equipe irá implamentar o que foi definido.</p>
						</div>
					</div>
					<div class="col-lg-4">
					    <div class="box">
							<div class="box-icon">
								<img src="img/service-icon-1.png" alt="">
							</div>
						<h4>Entrega e Feedback</h4>
                        <p>Ao final, seu sistema será entregue para  o feedback.</p>
						</div>
					</div>
				</div>
			</div>
	</section> 
	<br>


	<!-- team section start -->
	<section class="team-area " id="team">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="sec-title">
							<img src="img/nosso-time.png" alt="#nossotime"> <span class="sec-title-border"><span></span><span></span><span></span></span>
							
						</div>
					</div>
				</div>
				<br>
				<br>
				<div class="row form-group">
					<div class="col-lg-3 col-sm-6">
					    <div class="single-team-member">
							<div class="team-member-img">
								<img src="img/time/endi1.png" alt="team" style="max-width:74%; padding-top:5%;">
	
							</div>
							<div class="team-member-info">
								<a><h4>Endi Laiane</h4></a>
								<p>Front-End</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
					    <div class="single-team-member">
							<div class="team-member-img">
							<img src="img/time/anisio1.png" alt="team"  style="max-width:73%; padding-top:5%;">
					
						</div>
							<div class="team-member-info">
								<a ><h4>Anísio Santos</h4></a>
								<p>Analista de Sistemas</p>
								<p></p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
					    <div class="single-team-member">
							<div class="team-member-img">
								<img src="img/time/dorea1.jpg" alt="team"  style="max-width:80%">
								
							</div>
							<div class="team-member-info">
								<a><h4>Luís Dorea</h4></a>
								<p>Mobile</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
					    <div class="single-team-member">
							<div class="team-member-img">
							<img src="img/time/daniel1.png" alt="team"  style="max-width:69%; padding-top:5%;">
							</div>
							<div class="team-member-info padding-top:5%;">
								<a><h4>Daniel Queiroz</h4></a>
								<p>Back-End</p>
							</div>
						</div>
					</div>
				</div>
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-sm-6">
								<div class="single-team-member">
									<div class="team-member-img">
									<img src="img/time/lamek1.png" alt="team"  style="max-width:75%; padding-top:3%;">
									</div>
									<div class="team-member-info">
										<a ><h4>Lameck Sandro</h4></a>
										<p>Full Stack</p>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="single-team-member">
									<div class="team-member-img">
										<img src="img/time/lucas1.gif" alt="team"  style="max-width:80%">
									</div>
									<div class="team-member-info">
										<a ><h4>Lucas Silva</h4></a>
										<p>Mobile</p>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-6" >
								<div class="single-team-member"  >
									<div class="team-member-img"  >
										<img src="img/time/voce.jpg" alt="team"  style="max-width:78%" >	
									</div>
									<div class="team-member-info">
										<a ><h4>VOCÊ</h4></a>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
			<!-- team section end -->
  

		<!-- showcase section start -->
		<section class="showcase-area ptb-60">
			<!--<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="sec-title">
							<h2>Showcase<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
						</div>
					</div>
				</div>-->
				<!--<div class="row flexbox-center">
					<div class="col-lg-6">
						<div class="single-showcase-box">
							<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip </p>
							<a href="#" class="appao-btn appao-btn2">Read More</a>
						</div>
					</div>-->
					<!--<div class="col-lg-6">
						<div class="single-showcase-box">
							<img src="assets/img/showcase.png" alt="showcase">
						</div>
					</div>
				</div>-->
				<!--<div class="row flexbox-center">
					<div class="col-lg-6">
						<div class="single-showcase-box">
							<img src="assets/img/showcase2.png" alt="showcase">
						</div>
					</div>-->
				<!--	<div class="col-lg-6">
						<div class="single-showcase-box">
							<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip </p>
							<a href="#" class="appao-btn appao-btn2">Read More</a>
						</div>
					</div>-->
				</div>
			</div>
		</section><!-- showcase section end -->

		<!-- counter section start -->
		<!--<section class="counter-area ptb-80">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6">
					    <div class="single-counter-box">
							<i class="icofont icofont-heart-alt"></i>
							
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
					    <div class="single-counter-box">
							<i class="icofont icofont-protect"></i>
							
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
					    <div class="single-counter-box">
							<i class="icofont icofont-download-alt"></i>
							
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
					    <div class="single-counter-box">
							<i class="icofont icofont-trophy"></i>
							
						</div>
					</div>
					
				</div>
			</div>
		</section>-->
		
		<!-- testimonial section start -->
		<!--<section class="testimonial-area ptb-90">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="sec-title">
							<h2>Testimonials<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8 offset-lg-2">
						<div class="testimonial-wrap">
							<div class="single-testimonial-box">
								<div class="author-img">
									<img src="assets/img/author/author1.jpg" alt="author" />
								</div>
								<h5>Mary Balogh</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in </p>
								<div class="author-rating">
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
								</div>
							</div>
							<div class="single-testimonial-box">
								<div class="author-img">
									<img src="assets/img/author/author2.jpg" alt="author" />
								</div>
								<h5>Mary Balogh</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in </p>
								<div class="author-rating">
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
								</div>
							</div>
							<div class="single-testimonial-box">
								<div class="author-img">
									<img src="assets/img/author/author2.jpg" alt="author" />
								</div>
								<h5>Mary Balogh</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in </p>
								<div class="author-rating">
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
									<i class="icofont icofont-star"></i>
								</div>
							</div>
						</div>
						<div class="testimonial-thumb">
							<div class="thumb-prev">
								<div class="author-img">
									<img src="assets/img/author/author2.jpg" alt="author" />
								</div>
							</div>
							<div class="thumb-next">
								<div class="author-img">
									<img src="assets/img/author/author2.jpg" alt="author" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>-->
		<!-- testimonial section end -->
		
		<!-- counter section end -->
		<!-- pricing section start 
		<section class="pricing-area ptb-60" id="pricing">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="sec-title">
							<h2>Our Pricing Plan<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="single-pricing-box">
							<div class="pricing-top">
								<h4>Basic</h4>
								<p>Suitable for Freelancer</p>
							</div>
							<div class="price">
								<h1><span>$</span>99</h1>
								<p>Basic</p>
							</div>
							<div class="price-details">
								<ul>
									<li>Email Marketing</li>
									<li>Email Builder</li>
									<li>Client Testing</li>
									<li>Multiple Email Support</li>
									<li>Email Read Receipent</li>
									<li>2 User Free</li>
								</ul>
								<a class="appao-btn" href="#">Order Now</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="single-pricing-box">
							<div class="pricing-top">
								<h4>Pro</h4>
								<p>Suitable for Freelancer</p>
							</div>
							<div class="price">
								<h1><span>$</span>199</h1>
								<p>Basic</p>
							</div>
							<div class="price-details">
								<ul>
									<li>Email Marketing</li>
									<li>Email Builder</li>
									<li>Client Testing</li>
									<li>Multiple Email Support</li>
									<li>Email Read Receipent</li>
									<li>2 User Free</li>
								</ul>
								<a class="appao-btn" href="#">Order Now</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="single-pricing-box">
							<div class="pricing-top">
								<h4>Ultimate</h4>
								<p>Suitable for Freelancer</p>
							</div>
							<div class="price">
								<h1><span>$</span>299</h1>
								<p>Basic</p>
							</div>
							<div class="price-details">
								<ul>
									<li>Email Marketing</li>
									<li>Email Builder</li>
									<li>Client Testing</li>
									<li>Multiple Email Support</li>
									<li>Email Read Receipent</li>
									<li>2 User Free</li>
								</ul>
								<a class="appao-btn" href="#">Order Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>-->
		<!-- pricing section end -->
		
		<!-- download section start -->
		<!--<section class="download-area ptb-90">
				<div class="row">
					<div class="col-lg-12">
						<ul>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/android.png" alt="" style="width:70px;">
									</div>
									
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/docker.png" alt="" style="width:100px;">
									</div>
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/php.png" alt="" style="width:100px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/laravel.png" alt="" style="width:100px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/CSS3.png" alt="" style="width:70px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/python.png" alt="" style="width:100px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/html5.png" alt="" style="width:100px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/jquery.png" alt="" style="width:150px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/js.png" alt="" style="width:100px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/react.png" alt="" style="width:100px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/git.png" alt="" style="width:100px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/trello.png" alt="" style="width:90px;">
									</div>
						
								</a>
							</li>
							<li>
								<a href="#" class="download-btn flexbox-center">
									<div class="download-btn-icon">
									<img src="img/ferramentas/xd.png" alt="" style="width:90px;">
									</div>
						
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section> -->
		<!-- download section end -->
		<!-- blog section start 
		<section class="blog-area ptb-90" id="blog">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="sec-title">
							<h2>Our Latest Blog<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-6">
					    <div class="single-post">
							<div class="post-thumbnail">
								<a href="blog.html"><img src="assets/img/blog/blog1.jpg" alt="blog"></a>
							</div>
							<div class="post-details">
								<div class="post-author">
									<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
									<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
									<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
								</div>
								<h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
								<p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
					    <div class="single-post">
							<div class="post-thumbnail">
								<a href="blog.html"><img src="assets/img/blog/blog2.jpg" alt="blog"></a>
							</div>
							<div class="post-details">
								<div class="post-author">
									<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
									<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
									<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
								</div>
								<h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
								<p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 d-md-none d-lg-block">
					    <div class="single-post">
							<div class="post-thumbnail">
								<a href="blog.html"><img src="assets/img/blog/blog3.jpg" alt="blog"></a>
							</div>
							<div class="post-details">
								<div class="post-author">
									<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
									<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
									<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
								</div>
								<h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
								<p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		 blog section end -->

		 <br>
		 <br>
		 

		<!-- google map area start -->
		<center class="form-group">
			<img src="img/onde-estamos.gif" alt="onde estamos" style=" width:400px;"id="onde">
		</center>
		<br>
		<br>
		<div class="google-map" id="map-canvas"></div>
		<!-- google map area end -->
		<!-- footer section start -->
		<footer class="footer" id="contact">
			<div class="container">
				<div class="row">
                    <div class="col-lg-6">
						<div class="contact-form">
							<h4>Alguma Dúvida?</h4>
							<p>Entre em contato com a gente!</p>
							<p class="form-message"></p>
							{!! Form:: open(['route'=>'email.store', 'method' => 'POST']) !!}
				                <input type="text" name="nome" id="nome" placeholder="Nome">
				                <input type="email" name="email" id="email" placeholder="Email">
				                <textarea placeholder="Mensagem" name="mensagem" id="mensagem"></textarea>
				                <button type="submit" name="submit" onclick="enviar()">Enviar</button>
								{!!Form:: close()!!}
						</div>
                    </div>
                    <div class="col-lg-6">
						<div class="contact-address">
							<h4>Endereço</h4>
							<p></p>
							<ul>
								<li>
									<div class="contact-address-icon">
										<i class="icofont icofont-headphone-alt"></i>
									</div>
									<div class="contact-address-info">
										<a href="callto:#">(71) 99377-1555</a>
									</div>
								</li>
								<li>
									<div class="contact-address-icon">
										<i class="icofont icofont-envelope"></i>
									</div>
									<div class="contact-address-info">
										<a href="mailto:#">contato@temet.com.br</a>
									</div>
								</li>
								<li>
									<div class="contact-address-icon">
										<i class="icofont icofont-web"></i>
									</div>
									<div class="contact-address-info">
										<a href="www.temet.com.br">www.temet.com.br</a>
									</div>
								</li>
							</ul>
						</div>
                    </div>
				</div>
				<div class="row">
                    <div class="col-lg-12">
						<div class="copyright-area">
							<ul>
								<li><a href="https://www.facebook.com/devtemet/" target="_blank"><i class="icofont icofont-social-facebook"></i></a></li>
								<li><a href="https://www.linkedin.com/company/devtemet/" target="_blank"><i class="icofont icofont-brand-linkedin"></i></a></li>
								<li><a href="https://www.instagram.com/temetdev/" target="_blank"><i class="icofont icofont-social-instagram"></i></a></li>
							</ul>
							<p>&copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright <script>document.write(new Date().getFullYear());</script> TEMET | Desenvolvimeto Ágil 
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> </p>
						</div>
                    </div>
				</div>
			</div>
		</footer><!-- footer section end -->
		<a href="#" class="scrollToTop">
			<i class="icofont icofont-arrow-up"></i>
		</a>
		
		<!-- jquery main JS -->
		<script src="js/jquery.min.js"></script>
		<!-- Bootstrap JS -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Slick nav JS -->
		<script src="js/jquery.slicknav.min.js"></script>
		<!-- Slick JS -->
		<script src="js/slick.min.js"></script>
		<!-- owl carousel JS -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- Popup JS -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<!-- Counter JS -->
		<script src="js/jquery.counterup.min.js"></script>
		<!-- Counterup waypoints JS -->
		<script src="js/waypoints.min.js"></script>
	    <!-- YTPlayer JS -->
	    <script src="js/jquery.mb.YTPlayer.min.js"></script>
		<!-- jQuery Easing JS -->
		<script src="js/jquery.easing.1.3.js"></script>
		<!-- Gmap JS -->
		<script src="js/gmap3.min.js"></script>
        <!-- Google map api -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBbz41-brI-9smLW6K6Erl-p4fcFqVRBo&callback=initMap"></script>

		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<!-- Custom map JS -->
		<script src="js/custom-map.js"></script>
		<!-- WOW JS -->
		<script src="js/wow-1.3.0.min.js"></script>
		<!-- Switcher JS -->
		<script src="js/switcher.js"></script>
		<!-- main JS -->
        <script src="js/main.js"></script>
        <!-- banner-temet JS -->
		<script src="js/banner-temet.js"></script>
		<!-- time JS -->
		<script src="js/time.js"></script>
		
	</body>
</html>